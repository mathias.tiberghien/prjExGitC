#include <stdio.h>
#include <stdlib.h>

int main()
{
    int nb;
    int sum_notes =0;
    int i=0;
    int note_min = 21, note_max = -1;
    printf("Nombre d'élèvres:");
    scanf("%d", &nb);
    while(i<nb)
    {
        int note;
        printf("Note de l'élève %d (0-20):",i+1);
        scanf("%d", &note);
        if(note<0 || note>20)
        {
            printf("La note doit être comprise en tre 0 et 20. Recommencez.\n");
            continue;
        }
        if(note<note_min)
        {
            note_min = note;
        }
        if(note>note_max)
        {
            note_max = note;
        }
        sum_notes+=note;
        printf("L'élève %d %s admis.\n", i+1, note>=10? "est": "n'est pas");
        i++;
    }

    printf("Moyenne de la classe: %.2f\n", ((double)sum_notes)/nb);
    printf("Note la plus basse: %d\n", note_min);
    printf("Note la plus haute: %d\n", note_max);

}
